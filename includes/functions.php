<?php
include_once("include/mobile_device_detect.php");

/*
    防止sql注入,防止xss攻击，放在php第一行
        @author:yaofuyuan
        @date:2012-11-15 14:23:11
*/
function safeInput(){
       safeArray($_GET);
       safeArray($_POST);
       safeArray($_COOKIE);
}
/*
    处理数组中的sql注入字符和js代码
        @author:yaofuyuan
        @date:2012-11-15 14:23:11
*/
function safeArray(&$arr){
    foreach($arr as $k => $v){
                if(is_array($v)){
                     $arr[$k]=safeArray($v);
                      continue;
                }
        $arr[$k]=str_replace(array('<','>'),array('&lt;','&gt;'),addslashes($v));
    }    
}

function safeInsert($table,$arr){
	safeArray($arr);
	$sql = 
	$col="";
	$val="";
	foreach ($arr as $k => $v){
		$col.=$k.",";
		$val.= "'".$v."',";
	}
	$col=rtrim($col,",");
	$val=rtrim($val,",");
	return query("insert into $table ($col) values($val)");
}

function safeUpdate($table,$arr,$where){
	safeArray($arr);	
	$sql = "";
	foreach ($aArray as $k => $v){
		$sql .= $k . "='".$v."',";
	}
	$sql="UPDATE $aTableName SET ".rtrim($sql,",");
	if ( $where ){
    	$sql .= " WHERE ".$where;
	}
	return query($sql);
}

/*
	后台记录信息
	$syslogFile在config.php定义
*/
function systemLog($msg){
	global $syslogFile;
	//echo $syslogFile ;
	return error_log(date("[Y-m-d H:i:s]")."\t".$_SERVER['REMOTE_ADDR']."\t".$msg."\n",3,$syslogFile);
}
/*
	获取数据库连接
*/
function getConnection(){
  $conn=mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die(mysql_error());
	mysql_select_db(DB_DATABASE) or die(mysql_error());
	mysql_query("SET NAMES 'utf8'");	
	return $conn;
}
/*
	执行sql语句，执行出错则退出程序
	默认使用$conn做数据库连接
*/
function query($sql){
	$res=mysql_query($sql) or systemLog($sql."\t".mysql_error()); 
	return $res;
}

function startTransaction(){
	query("START TRANSACTION");	
}

function commitTransaction(){
	query("COMMIT");	
}

function rollback(){
	query("ROLLBACK");	
}

/*
	查询某表的所有记录
*/
function getAllRows($table){
	$res=query("select * from $table"); 
	$result=array();
	while($row=mysql_fetch_assoc($res)){
		$result[]=$row;
	}
	return $result; 
}


/*
	分页查询车队
*/
function getQueueByPage($page){
	$startPos=($page-1)*PAGE_SIZE;
	$res=query("select id,weibo_screen_name,weibo_icon,car_id,prize_id from car_queue order by id desc limit $startPos,".PAGE_SIZE); 
	$result=array();
	while($row=mysql_fetch_assoc($res)){
		$row['short_name']=getShortName($row['weibo_screen_name']);
		$result[]=$row;
	}
	return $result; 
}

//是否已经赢得过奖品
function isWined($weiboUid){
	systemLog("select count(*) from car_queue where weibo_uid='$weiboUid' and prize_id>0");
	if($res=query("select count(*) from car_queue where weibo_uid='$weiboUid' and prize_id>0")){
		if($row=mysql_fetch_array($res)){
			if($row[0]==0){
				return false;
			}else{
				return true;
			}
		}
	}
	return true;
}


//是否需要填写用户信息
function isNeedWinnerInfo($weiboUid){
	if($res=query("select win_uuid from car_queue where weibo_uid='$weiboUid' and win_uuid!='' and win_uuid not in (select win_uuid from winner)")){
		if($row=mysql_fetch_array($res)){
			return $row[0];
		}else{
			return "false";
		}
	}
	return "false";
}

function isInCity($province_id,$city_id){
	$res=query("select count(*) from ticket_city where province_id='$province_id' or city_id='$city_id'"); 
	if($row=mysql_fetch_array($res)){
		if($row[0]==0){
			return false;
		}else{
			return true;
		}
	}
}

//是否需要填写用户信息
function getPrizes(){
	global $prizes;
	//缓存
	if(is_array($prizes)&& count($prizes) > 0){
		return 	$prizes;
	}
	if($res=query("select * from prize order by id asc")){
		while($row=mysql_fetch_assoc($res)){
			$prizes[]=$row;
		}
	}
	return $prizes;
}

function verifyMobile($mobile){ 
	//11位手机号  
	return preg_match("/^1[3|5|8]\d{9}$/",$mobile);
} 

function verifyEmail($email){  
	//Email地址   
	return preg_match('/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/', $email);
}

function verifyWeiboNickname($name){
	//4-30个字符，支持中英文、数字、"_"或减号
	if(strlen($name)<4){
		return false ;
	}else{
		return true ;
	}
}

function jumpTo($url){
	echo "<script> top.location='$url';</script>";
}
function uuid($prefix = ''){
     $chars = md5(uniqid(mt_rand(), true));
     $uuid= substr($chars,0,8) . '-';
     $uuid.= substr($chars,8,4) . '-';
     $uuid.= substr($chars,12,4) . '-';
     $uuid.= substr($chars,16,4) . '-';
     $uuid.= substr($chars,20,12);
     return $prefix . $uuid;
} 
//生成随机数
function randomkeys($length) {
    $returnStr='';
    $pattern = '1234567890ABCDEFGHIJKLOMNOPQRSTUVWXYZ';
    for($i = 0; $i < $length; $i ++) {
        $returnStr .= $pattern {mt_rand ( 0, 35 )}; //生成php随机数
    }
    return $returnStr;
}

function isWin($weiboUid,$province_id,$city_id){
	global $prizes;
	//return "false";//不让中奖
	if(isWined($weiboUid)){
		systemLog("$weiboUid already wined.");
		return array("status"=> false,"winUuid"=>"","prizeId"=> 0);	
	}
	$prizes=getPrizes();
	$winUuid=0;
	foreach($prizes as $prize){
		if($prize['left_num']==0){
			continue;
		}
		$prizeId=$prize['id'];
		if($prizeId==1){
			//判断若门票时必须是上海周边城市
			if(!isInCity($province_id,$city_id)){
				continue;
			}
		}
		$percent=$prize['percent']*RAND_MAX;
		//产生一个1到100000的随机数
		$randKey = mt_rand(1,RAND_MAX);	
		//当随机数小于设定的几率时认为是中奖
		systemLog("$randKey <= $percent");
		if($randKey <= $percent){	
			$winUuid=uuid();
			//奖品数减一
			query("update prize set left_num=left_num-1 where id=$prizeId");
			if(mysql_affected_rows() ==1){
				return array("status"=> true,"winUuid"=> $winUuid,"prizeId"=> $prize['id'],"prize"=> $prize['name']);
			}else{
				return array("status"=> false,"winUuid"=>"","prizeId"=> 0);	
			}
		}
	}
	return array("status"=> false,"winUuid"=>"","prizeId"=> 0);	
}




function getShortName($longName){
	//英语
	if(mb_strlen($longName,"UTF-8")==strlen($longName)){
		//中文yifangyou
		if(mb_strlen($longName,"UTF-8")>9){
				return mb_substr($longName,0,8,"UTF-8")."...";
		}else{
				return $longName;
		}
	}else
	if(mb_strlen($longName,"UTF-8")>5){
			//中文
			return mb_substr($longName,0,4,"UTF-8")."...";
	}else{
			return $longName;
	}
}

function getGeo($ip){
	$ipstr=sprintf("%u", ip2long($ip));
	if($res=query("select province_id,city_id from ip_db where start_ip<=$ipstr and end_ip>=$ipstr")){
		if($row=mysql_fetch_assoc($res)){
			if($row["province_id"]==0){
				$row["province_id"]=-1;
			}
			if($row["city_id"]==0){
				$row["city_id"]=-1;
			}
			return array($row["province_id"],$row["city_id"]);
		}
	}
	return array(-1,-1);
}

function getIp(){
	$ip=false;
	if(!empty($_SERVER["HTTP_CLIENT_IP"])){
  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	}
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
  	if($ip){
   		array_unshift($ips, $ip); $ip = FALSE;
  	}
  	for($i = 0; $i < count($ips); $i++){
   		if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
    		$ip = $ips[$i];
    		break;
   		}
  	}
	}
	return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}


/*大众广州车展*/

function saveAttendance($user,$isfollow){
		list($is_mobile,$mobile_type)=mobile_device_detect();
		$attCount = 0;
		$res = query("select count(*)  from vw_attendance where date(create_date)=date_sub('".date("Y-m-d")."',interval 0 day) and user_id = '".$user['id']."'");
		//mysql_fetch_array
		//mysql_fetch_assoc
		if($row=mysql_fetch_array($res)){
			$attCount=$row[0];
		}
		if($attCount>0){
			return "-1";//签过了，请明天再来吧！	
		}else{
			$sql = "insert into vw_attendance(user_name,user_id,isfollow,ip,create_date,mobile_type) values('".$user['screen_name']."','".$user['id']."','".$isfollow."','".getIp()."','".date("Y-m-d H:i:s")."','".$mobile_type."')";
			return query($sql);
		}
}

/*分享*/
function saveShareInfo($user,$shareImgId){
		list($is_mobile,$mobile_type)=mobile_device_detect();
		$sql = "insert into vw_shareInfo(user_name,user_id,share_image_id,ip,create_date,mobile_type) values('".$user['screen_name']."','".$user['id']."','".$shareImgId."','".getIp()."','".date("Y-m-d H:i:s")."','".$mobile_type."')";
		if(query($sql)){
			$res = query("select count(*)  from vw_shareInfo where share_image_id = ".$shareImgId."");
			$row=mysql_fetch_array($res);
			return $row[0];
		}
}

/*获取分享次数*/
function getAlldata(){
	$res=query("select * from  vw_autoshow_images  as au left join (select share_image_id,count(*)  as count from vw_shareInfo group by share_image_id) as sh on sh.share_image_id=au.id order by create_date desc");
	while($row=mysql_fetch_assoc($res)){
		$list[]=$row;
	}
	
	return $list;	
}
function getAllautoshowImg(){
	$pageSize = PAGESIZE;
	$page = $_GET['page'] ? addslashes($_GET['page']):1;
	$pageIndex =($page -1)*$pageSize;
	$sql = 'select * from  vw_autoshow_images  as au left join (select share_image_id,count(*)  as count from vw_shareInfo group by share_image_id) as sh on sh.share_image_id=au.id order by au.id desc limit '.$pageIndex.','.$pageSize;
	
	$resouce = query($sql);
	
    while($row = mysql_fetch_assoc($resouce)) {
        $data[] = $row;
    }
	
    $totalCount = count(getAlldata());
    $allData = array();
    $currentPage = $page;    //当前页
    if(!isset($currentPage))
        $currentPage = 1;

    if($end > $totalCount){
        $end = -1;
    }
    $pageCount = ceil($totalCount/$pageSize);//总页数
    $prevPage = $currentPage-1;//上一页
    if($prevPage < 1)
        $prevPage = 1;

    $nextPage = $currentPage+1;//下一页
    if($nextPage > $pageCount)
        $nextPage = $pageCount;

    $totalpage = ceil($totalCount/$pageSize);


    $pageInfo = array(
                    "pageSize" => $pageSize,
                    "totalCount" => $totalCount,
                    "currentPage" => $currentPage,
                    "firstPage" => 1 ,
                    "lastPage" => $totalCount,
                    "prevPage" => $prevPage,
                    "nextPage" => $nextPage,
                    "totalPage" =>$totalpage
                );
	
   $allData['data'] = $data;
   $allData['pageInfo'] = $pageInfo;
   
   return $allData;
};
function getallwinners(){
	$res=query("select * from  vw_winnersList order by date desc");
	$dates = array();
	while($row=mysql_fetch_assoc($res)){
		extract($row);
		$list[$date][]=$winnername;
	}
	$dates = array_keys($list);
	sort($dates);
	return array($dates,$list);	
}

?>