<?php
error_reporting(0) ;
session_start();

//欧维特对接
function ouweiteTestDriver($driverInfo,$key){
	$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
	$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
	$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
	$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
	$client = new nusoap_client('http://guide.dongfeng-nissan.com.cn:9080/nissanlms/services/CampaignWService?wsdl', 'wsdl',
							$proxyhost, $proxyport, $proxyusername, $proxypassword);
	
	$client->soap_defencoding = 'UTF-8';    //解决php访问webserver中文乱码的问题
	$client->decode_utf8 = false;	 //解决php访问webserver中文乱码的问
							
	$err = $client->getError();
	if ($err) {
		//echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
		return $err ;
	}
	//查找dealerId
	//$dealer = $driverInfo[shop] ;
	$dealerInfo = tFindDealerId($driverInfo[shop]) ;
	// Doc/lit parameters get wrapped
	$param = array() ;
	if($dealerInfo){
		$param = array(
					'customerName' => $driverInfo[name],
					'mobile'=>$driverInfo[mobile],
					'serialNumbers'=>$key,
					'dealerId'=>$dealerInfo['no'],
					'seriesId'=>24,
					'isApplyTry'=>'是',//是否申请试驾
					'provinceId'=>$dealerInfo['province_id'],
					'cityId'=>$dealerInfo['city_id']					
				);
		//1.2.1方法 insertCampaignNew（专营店编号必填）	
		//1.2.3方法insertCampaignRetLead		
		$method = "insertCampaignRetLead" ;
	}else{
		$param = array(
					'customerName' => $driverInfo[name],
					'mobile'=>$driverInfo[mobile],
					'serialNumbers'=>$key,
					'seriesId'=>24,
					'isApplyTry'=>'是',//是否申请试驾
					'provinceId'=>$dealerInfo['province_id'],
					'cityId'=>$dealerInfo['city_id']
				);
		//1.2.1方法 insertCampaignForECA（不填专营店编号）
		$method = "insertCampaignForECA" ;
	}
	//foreach($param as $k=>$v) if(is_string($v)) $post[$k]=iconv('gbk','utf-8',$v);
	$result = $client->call($method, array($param), '', '', false, true);
	// Check for a fault
	if ($client->fault) {
		return $result ;
	} else {
		// Check for errors
		$err = $client->getError();
		if ($err) {
			// Display the error
			return $err ;
		} else {
			// Display the result
			return $result ;
		}
	}	
}
//根据经销商名称查找dealerId
function tFindDealerId($dealer){
	$sql = "SELECT province_id,city_id,no FROM dealer WHERE dealer = '".$dealer."'" ;
	$res = query($sql) ;
	if($row=mysql_fetch_array($res)){
		return $row ;
	}else{
		return false ;
	}	
}

//预约试驾
function tSaveTestDriver($driverInfo){
	$sql = "insert into test_driver(name,mobile,email,address,province,city,shop,test_time,is_want_buy,source,create_date) ".
	"values('".$driverInfo[name]."','".$driverInfo[mobile]."','".$driverInfo[email]."','".$driverInfo[address]."','".$driverInfo[province]."'" .
			",'".$driverInfo[city]."','".$driverInfo[shop]."','".$driverInfo[test_time]."','".$driverInfo[is_want_buy]."','".$driverInfo[source]."','".date("Y-m-d H:i:s")."')";
	//systemLog($sql) ;
	if(query($sql)){
		return true ;
	}else{
		return false ;
	}	
}
function tDealerList($province,$city){	
	$sql = "SELECT city,province, dealer shop,address,service_phone,sale_phone FROM dealer " ;
	if(!empty($city)){
		$sql .= "WHERE city = '".$city."'" ;
	}elseif(!empty($province)){
		$sql .= "WHERE province = '".$province."'" ;
	}
	systemLog("sql:"+$sql) ;
	$res = query($sql) ;
	$result=array();
	while($row=mysql_fetch_assoc($res)){
		//处理电话
		if($pos = strpos($row['service_phone'],",")){
			$row['service_phone'] = substr($row['service_phone'],0,$pos) ;
		}
		if($pos = strpos($row['sale_phone'],",")){
			$row['sale_phone'] = substr($row['sale_phone'],0,$pos) ;
		}
		//处理地址
		$len = 20 ;
		/*
		if(utf8_strlen($row['address'])>$len){
			$row['address'] = cut_str($row['address'], $len, 0, 'UTF-8');  ;				
		}*/
		
				
		$result[]=$row;
	}
	return $result;
}
// 计算中文字符串长度
function utf8_strlen($string = null) {
	// 将字符串分解为单元
	preg_match_all("/./us", $string, $match);
	// 返回单元个数
	return count($match[0]);
}
//截取字符串 支持UTF-8、GB2312
function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') {
	if ($code == 'UTF-8') {
		$pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
		preg_match_all($pa, $string, $t_string);
		if (count($t_string[0]) - $start > $sublen)
			return join('', array_slice($t_string[0], $start, $sublen)) . "...";
		return join('', array_slice($t_string[0], $start, $sublen));
	} else {
		$start = $start * 2;
		$sublen = $sublen * 2;
		$strlen = strlen($string);
		$tmpstr = '';
		for ($i = 0; $i < $strlen; $i++) {
			if ($i >= $start && $i < ($start + $sublen)) {
				if (ord(substr($string, $i, 1)) > 129) {
					$tmpstr .= substr($string, $i, 2);
				} else {
					$tmpstr .= substr($string, $i, 1);
				}
			}
			if (ord(substr($string, $i, 1)) > 129)
				$i++;
		}
		if (strlen($tmpstr) < $strlen)
			$tmpstr .= "...";
		return $tmpstr;
	}
}
?>
